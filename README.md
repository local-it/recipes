# recipes.coopcloud.tech

[![Build Status](https://build.coopcloud.tech/api/badges/coop-cloud/recipes-catalogue-json/status.svg?ref=refs/heads/main)](https://build.coopcloud.tech/coop-cloud/recipes-catalogue-json)

> [recipes.coopcloud.tech/recipes.json](https://recipes.coopcloud.tech)

The recipe catalogue for [Co-op Cloud](https://coopcloud.tech).

## Automatic deploy

Each commit to this repo is deployed to [recipes.coopcloud.tech/recipes.json](https://recipes.coopcloud.tech).

Commits can be sent by running `abra catalogue generate --publish` (requires write permissions + ssh keys setup).

## Manual deploy

`make`
