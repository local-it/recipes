STACK := recipes_coopcloud_tech_json

default: deploy

generate:
	abra catalogue generate

deploy:
	@DOCKER_CONTEXT=mellor.coopcloud.tech docker stack rm $(STACK) && \
		DOCKER_CONTEXT=mellor.coopcloud.tech docker stack deploy -c compose.yml $(STACK) && \
		DOCKER_CONTEXT=mellor.coopcloud.tech docker service scale recipes_coopcloud_tech_json_app=0 && \
		DOCKER_CONTEXT=mellor.coopcloud.tech docker service scale recipes_coopcloud_tech_json_app=1

.PHONY: generate deploy
